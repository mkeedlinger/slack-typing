use anyhow::{Context, Result};
use slack::{Event, EventHandler, RtmClient};
use std::env::var;

fn main() -> Result<()> {
    let token = var("SLACK_TOKEN").context("Expected 'SLACK_TOKEN' env var")?;

    let client = RtmClient::login(&token).context("Failed to log in")?;

    client
        .run(&mut Handler {})
        .context("Failed to start handler")?;
    Ok(())
}

struct Handler;
impl EventHandler for Handler {
    fn on_event(&mut self, cli: &RtmClient, event: slack::Event) {
        dbg!(&event);

        match event {
            Event::UserTyping { channel, user: _ } => {
                match cli.sender().send_typing(&channel) {
                    Ok(_) => {}
                    Err(err) => {
                        println!("Failed to respond: {}", err);
                    }
                };
            }
            _ => {}
        }
    }

    fn on_close(&mut self, _cli: &RtmClient) {
        dbg!()
    }

    fn on_connect(&mut self, _cli: &RtmClient) {
        dbg!()
    }
}
